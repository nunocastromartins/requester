# Requester #
Use it to test your REST API: [http://requester.aerobatic.io](http://requester.aerobatic.io/ "Requester")

Either [CORS](http://enable-cors.org/server.html "Enable CORS") or [JSON-P](http://json-p.org/ "JSON-P") must be used in order to be able to use this application externally, otherwise use it from within the server.
